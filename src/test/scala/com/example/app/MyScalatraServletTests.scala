package com.example.app

import org.scalatra.test.scalatest._

class MyScalatraServletTests extends ScalatraFunSuite {

  val CONTAINER_01 = "CONTAINER-X01"
  val CONTAINER_03 = "CONTAINER-X03"
  val CONTAINER_UNKNOWN = "CONTAINER-unknown"

  addServlet(classOf[MyScalatraServlet], "/*")

  test("GET / on MyScalatraServlet should return status 200") {
    get("/") {
      status should equal (200)
    }
  }

  test("GET /:containerId with unknown container should return status 404") {
    get("/" + CONTAINER_UNKNOWN) {
      status should equal (404)
    }
  }

  test("GET /:containerId on MyScalatraServlet should return status 200") {
    get("/" + CONTAINER_01) {
      status should equal (200)
      // TODO - this is probably a pretty naive way to do test the result
      body should be ("""[{"id":"1","sku":"SKU-01","qty":1,"containerId":"CONTAINER-X01"}]""")
    }
  }

  test("POST / and GET:/containerId on the new inventory record") {
    val newRecordString = """{"id":"4","sku":"SKU-01","qty":1,"containerId":"CONTAINER-X03"}"""
    post("/", body = newRecordString) {
      status should equal (200)
    }

    get("/" + CONTAINER_03) {
      status should equal (200)
      body should be ("[" + newRecordString + "]")
    }
  }

  test("POST /asyncTest to experiment with Futures") {
    var i = 0

    // TODO - I don't know how to do for loops (specifically, how to get a range)
    while (i < 10) {
      post("/asyncTest") {
        status should equal(200)
      }
      i += 1
    }
  }



}
