package com.example.app

import org.slf4j.{Logger, LoggerFactory}

import org.json4s.{DefaultFormats, Formats}
import org.scalatra._
import org.scalatra.json._

import scala.collection.mutable.ListBuffer
import scala.concurrent.{ExecutionContext, Future}


class MyScalatraServlet extends ScalatraServlet with NativeJsonSupport with FutureSupport {

  // TODO - how do do real logging? ANSWER: http://scalatra.org/guides/2.3/monitoring/logging.html
  val logger =  LoggerFactory.getLogger(getClass)

  protected implicit val jsonFormats: Formats = DefaultFormats

  before() {
    contentType = formats("json")
  }

  get("/") {
    InventoryDataStore.data
  }

  get("/:containerId") {
    val containerId = params("containerId")
    // I don't know how to match on list result, not sold that a 404 is the way to do this,
    // but I mostly just want to practice dealing with the results of the lookup
    val result = InventoryDataStore.data.filter(_.containerId == containerId)
    if (result.size >= 1) { result }
    else {
      status = 404
      NotFound
    }
  }

  post("/") {
    log("Someone attempted to POST")
    // TODO - how do I do even basic validation?
    val rawRecord = parsedBody.extract[InventoryRecord]
    InventoryDataStore.data.append(rawRecord)
  }

  protected implicit def executor = ExecutionContext.global

  post("/asyncTest") {
    logger.info("testing Futures framework")
    Future {
      Grabber.evaluate("test")
    }
  }

}

object Grabber {
  val logger =  LoggerFactory.getLogger(getClass)

  def evaluate(url: String): String = {
    logger.info("evaluating in the future")
    "Foobar" + url
  }
}

case class InventoryRecord(id: String, sku: String, var qty: Int, containerId: String)

object InventoryDataStore {
  val data = ListBuffer[InventoryRecord](
    InventoryRecord("1", "SKU-01", 1, "CONTAINER-X01"),
    InventoryRecord("2", "SKU-01", 1, "CONTAINER-X02"),
    InventoryRecord("3", "SKU-02", 1, "CONTAINER-X02"),
  )
}